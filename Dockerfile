FROM --platform=linux/amd64 node:18

WORKDIR /app

COPY package.json package.json
COPY package-lock.json package-lock.json
COPY ./src ./src
COPY ./tsconfig.json ./tsconfig.json

RUN npm ci

ENV NODE_ENV=production

RUN npm run build

EXPOSE 3000

CMD [ "npm", "run", "prod" ]