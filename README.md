# Action Item: Deployment to `ECS` 📦

In this `Action Item` you will:

-   🚀 Deploy our `API` to `ECS` manually
-   📦 Use ECR(Elastic Container Repository) to store our `Docker` images
-   🗂️ Use `SSM` to store and distribute secrets
-   🤖 Use `GitLab` to fully automatize the `container` deployment

## Challenges:

1. [x] 🧙🏽[EXPERT] Create an `ECR Repository` and push the `API Docker Image`
2. [ ] 🧙🏽[EXPERT] Create an `ECS Cluster` where we will deploy our `containers`
3. [ ] 🧙🏽[EXPERT] Add `endpoints` to your `VPC` to calls to other `AWS` Services
4. [ ] 🧙🏽[EXPERT] Create a `Security Group` for the `ECS Task`
5. [ ] 🧙🏽[EXPERT] Create an `ECS Task Definition`
6. [ ] 🧙🏽[EXPERT] Deploy the `Task Definition` using an `ECS Service`
7. [ ] 🧙🏽[EXPERT] `Automate` the deployment with `GitLab`

> Make sure you set aside at least **2 hours** of focused, uninterrupted work and give your best.

---

### <a name="app-structure"></a> App Architecture

![app-structure](examples/app_structure/app_structure.png)

---

#### ⚠️ Avoid Accidental Charges from AWS ⚠️

In this `Action Item` we will use AWS service in their [Free Tier](https://aws.amazon.com/free/?all-free-tier.sort-by=item.additionalFields.SortRank&all-free-tier.sort-order=asc&awsf.Free%20Tier%20Types=*all&awsf.Free%20Tier%20Categories=categories%23databases) that are offered free of charge inside certain limits. To make sure you wont be charged by AWS:

1) Check the Billing Dashboard reglarry - [How to use the Billling Dashboard](https://www.loom.com/share/0d55730aa26d4a4f947cc4a89c1b4408)
2) Setup a Budget and Email alerts for yourself - [How to setup a Budget in AWS](https://www.loom.com/share/2cb3a3e78d10477991fa0e3429c9ff00)

#### Clean Up After the Action Item:

1) [Delete the **ECS Cluster** - Video Guide](https://www.loom.com/share/4f2af16548594dc8b67ca4fe4250dea2)
2) [Delete the **ECR Repository** - Video Guide](https://www.loom.com/share/27fa0ed1d3dd407faf800156ef8f8764)
3) [Delete the **EC2 Load Balancer** - Video Guide](https://www.loom.com/share/52484a5b148243babca7f9bc8e35e39c)
4) [Delete the **VPC Endpoints** - Video Guide](https://www.loom.com/share/545a211814a14ec8b7e903fa8b1db935)

#### ⚠️ Make sure you check the video above to avoid any unwanted charges at the end of the month! ⚠️

---

<details closed>
<summary>CLICK ME! - SOLUTION: 1. Create an ECR Repository and push the Docker Image</summary>

#### 1. Create an `ECR` repository to store `Docker` images

##### 1.1 Go to [AWS Elastic Container Registry](https://aws.amazon.com/ecr/)

##### 1.2. Create a `private docker repository`

![create-repository](examples/ecr_repository/create-repository-ecr.png)

![private-repository-create](examples/ecr_repository/ecr.png)

##### 1.3. Create a `CLI` user that you can use to push images to `ECR`

-   1.3.0 Install the [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
-   1.3.1 Go to [IAM](https://us-east-1.console.aws.amazon.com/iamv2/home?region=eu-central-1)
-   1.3.2 Head over to [IAM Users](https://us-east-1.console.aws.amazon.com/iamv2/home?region=eu-central-1#/users)
-   1.3.3 Create a new user with the permission to push to `ECR`
    ![permission-policy-user](examples/ecr_repository/user-policy-ecr.png)

-   1.3.4 Save the credentials of this new user in a note
-   1.3.5. In your terminal run:

    ```bash
    aws configure
    ```

    And follow the instructions.

##### 1.4. `Dockerize` the `API`

At `root` level in the directory create a new file called `Dockerfile`:

```docker
FROM --platform=linux/amd64 node:16

WORKDIR /app

COPY package.json package.json
COPY ./src ./src
COPY ./tsconfig.json ./tsconfig.json

RUN npm install

ENV NODE_ENV=production

RUN npm run build

EXPOSE 3000

CMD [ "npm", "run", "prod" ]
```

Build the `docker image`. In your terminal:

```bash
docker build .
```

You should get an `image id` once the build is done:
![image-id](examples/ecr_repository/image_id.png)

And you can run the image locally in your terminal:

```bash
docker run -p 3000:3000 --env-file .env.production IMAGE_ID
```

⚠️Make sure there is nothing else running on port 3000 and that you have a `.env.production` file at the root level.⚠️

##### 1.5. Build an image of the `API` service and push it to `ECR`

Once the `image` works locally we can push it to `ECR`. Follow the instructions under **"View push commands"**:
![view-push-commands](examples/ecr_repository/ecr_push_commands.png)

Yours should look something like this:

![example-push-commands](examples/ecr_repository/ecr_commands_example.png)

Once you run them, your image should be on `ECR` with the tag `latest`.

-   [Video Solution - Create an `ECR Repository` and push the `API` docker image](https://www.loom.com/share/3ab45ebf78714d05bb7a536f6950886e)

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 2. Create an ECS Cluster</summary>

### 2. Create an `ECS Cluster`

-   [Video Solution - Create an `ECS Cluster`](https://www.loom.com/share/0141b9cc96fa4b4f9f5b97a3818aea56)

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 3. Add endpoints to your VPC to allow ECS calls outside</summary>

### 3. Add `endpoints` to allow calls outside your `VPC` safety

##### 3.1 Go to [AWS VPC](https://eu-central-1.console.aws.amazon.com/vpc/home?region=eu-central-1#Home:)

##### 3.2 In the `sidebar` navigate to endpoints:

![select-endpoints](examples/vpc_endpoints/select_endpoints.png)

##### 3.3 Create the endpoints with the following settings:

![step_one](examples/vpc_endpoints/endpoint_step_1.png)
![step_two](examples/vpc_endpoints/endpoint_step_2.png)
![step_three](examples/vpc_endpoints/endpoint_step_3.png)
![step_four](examples/vpc_endpoints/endpoint_step_4.png)
![step_five](examples/vpc_endpoints/endpoint_step_5.png)
![step_siz](examples/vpc_endpoints/endpoint_step_6.png)

#### [Video Solution - Add endpoints needed by `ECS` to your VPC](https://www.loom.com/share/33311d54b8bb4afca5243e270417da4b)

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 4. Create a Security Group for the ECS Task</summary>

### 4. Create a Security Group for the ECS Task

> NOTE: A security group is a "virtual firewall" that we can attach to resources in AWS to control the inbound and outbound traffic(the traffic that comes in and the one that goes out).

##### 4.1 Go to [Security Groups](https://eu-central-1.console.aws.amazon.com/ec2/home?region=eu-central-1#SecurityGroups:)

##### 4.2 Click "Create Security Group"

![click-create-sec](examples/security_group/create-sec-group.png)

##### 4.3 Add the following `Inbound Rules`

![click-create-sec](examples/security_group/inbound-rules.png)

#### [Video Solution - Add a Security Group for the ECS Task](https://www.loom.com/share/f168d3704c1340d498a10961c6bed802)

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 5. Create an ECS Task Definition</summary>

#### 5. Create an ECS Task Definition

##### 5.1 Save the `API environment variables` in Parameter Store

-   Go to [Parameter Store](https://eu-central-1.console.aws.amazon.com/systems-manager/parameters/?region=eu-central-1&tab=Table)

-   Add the env variables to `Parameter Store`
    ![parameter-store-setup](examples/parameter-store-setup.png)

#### [Video Solution - Add the env variables to Parameter Store](https://www.loom.com/share/f685063a5d8d4b19a6515466ad140174)

##### 5.2 Create an `ECS Task Definition`In the environment `variables section` add the `ARN` of each corresponding parameter following the pattern below:

```bash
// Pattern
arn:aws:ssm:REGION:ACCOUNT_ID:parameter/PARAMETER_NAME

// example
arn:aws:ssm:eu-central-1:444265927816:parameter/AUTH_AUDIENCE
```

> For more details [see the official documentation here](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/specifying-sensitive-data-parameters.html#secrets-envvar-parameters)

You can get your `Account Id` here:
![AWS-account-id](examples/aws_account_id.png)

And the `region` you are working in here:
![AWS-region](examples/aws_region_id.png)

#### [Video Solution - Create an `ECS Task Definition` to specify how our container will be deployed](https://www.loom.com/share/bb9ddeb5480c4471a6c99a06861ef259)

> NOTE: you can add a **healthcheck** to your container that ECS will use to verify the containers in your tasks are **HEALTHY**.

```bash
CMD-SHELL, curl -f http://localhost:3000/healthcheck || exit 1
```

> NOTE: the `/healthcheck` endpoint must be present in your app - we already added that for you.

The `JSON` version of our `TASK Definition` looks like this:

```json
{
    "taskDefinitionArn": "arn:aws:ecs:eu-central-1:444265927816:task-definition/MovieApiProduction:2",
    "containerDefinitions": [
        {
            "name": "movie-api-production",
            "image": "444265927816.dkr.ecr.eu-central-1.amazonaws.com/movie-api",
            "cpu": 0,
            "links": [],
            "portMappings": [
                {
                    "name": "movie-api-production-3000-tcp",
                    "containerPort": 3000,
                    "hostPort": 3000,
                    "protocol": "tcp",
                    "appProtocol": "http"
                }
            ],
            "essential": true,
            "entryPoint": [],
            "command": [],
            "environment": [
                {
                    "name": "DB_USERNAME",
                    "value": "arn:aws:ssm:eu-central-1:444265927816:parameter/MOVIE_API_PRODUCTION_DB_USERNAME"
                },
                {
                    "name": "DB_PORT",
                    "value": "arn:aws:ssm:eu-central-1:444265927816:parameter/MOVIE_API_PRODUCTION_DB_PORT"
                },
                {
                    "name": "DB_HOST",
                    "value": "arn:aws:ssm:eu-central-1:444265927816:parameter/MOVIE_API_PRODUCTION_DB_HOST"
                },
                {
                    "name": "AUTH_AUDIENCE",
                    "value": "arn:aws:ssm:eu-central-1:444265927816:parameter/AUTH_AUDIENCE"
                },
                {
                    "name": "AUTH_TENANT_URL",
                    "value": "arn:aws:ssm:eu-central-1:444265927816:parameter/AUTH_TENANT_URL"
                },
                {
                    "name": "DB_PASSWORD",
                    "value": "arn:aws:ssm:eu-central-1:444265927816:parameter/MOVIE_API_PRODUCTION_DB_PASSWORD"
                }
            ],
            "environmentFiles": [],
            "mountPoints": [],
            "volumesFrom": [],
            "secrets": [],
            "dnsServers": [],
            "dnsSearchDomains": [],
            "extraHosts": [],
            "dockerSecurityOptions": [],
            "dockerLabels": {},
            "ulimits": [],
            "logConfiguration": {
                "logDriver": "awslogs",
                "options": {
                    "awslogs-create-group": "true",
                    "awslogs-group": "/ecs/MovieApiProduction",
                    "awslogs-region": "eu-central-1",
                    "awslogs-stream-prefix": "ecs"
                },
                "secretOptions": []
            },
            "healthCheck": {
                "command": [
                    "CMD-SHELL",
                    " curl -f http://localhost:3000/healthcheck || exit 1"
                ],
                "interval": 30,
                "timeout": 5,
                "retries": 3,
                "startPeriod": 120
            },
            "systemControls": []
        }
    ],
    "family": "MovieApiProduction",
    "executionRoleArn": "arn:aws:iam::444265927816:role/ecsTaskExecutionRole",
    "networkMode": "awsvpc",
    "revision": 2,
    "volumes": [],
    "status": "ACTIVE",
    "requiresAttributes": [
        {
            "name": "com.amazonaws.ecs.capability.logging-driver.awslogs"
        },
        {
            "name": "ecs.capability.execution-role-awslogs"
        },
        {
            "name": "com.amazonaws.ecs.capability.ecr-auth"
        },
        {
            "name": "com.amazonaws.ecs.capability.docker-remote-api.1.19"
        },
        {
            "name": "com.amazonaws.ecs.capability.docker-remote-api.1.17"
        },
        {
            "name": "ecs.capability.container-health-check"
        },
        {
            "name": "ecs.capability.execution-role-ecr-pull"
        },
        {
            "name": "com.amazonaws.ecs.capability.docker-remote-api.1.18"
        },
        {
            "name": "ecs.capability.task-eni"
        },
        {
            "name": "com.amazonaws.ecs.capability.docker-remote-api.1.29"
        }
    ],
    "placementConstraints": [],
    "compatibilities": ["EC2", "FARGATE"],
    "requiresCompatibilities": ["FARGATE"],
    "cpu": "1024",
    "memory": "3072",
    "runtimePlatform": {
        "cpuArchitecture": "X86_64",
        "operatingSystemFamily": "LINUX"
    },
    "registeredAt": "2022-12-12T17:25:53.548Z",
    "registeredBy": "arn:aws:iam::444265927816:root",
    "tags": [
        {
            "key": "ecs:taskDefinition:createdFrom",
            "value": "ecs-console-v2"
        },
        {
            "key": "ecs:taskDefinition:stackId",
            "value": "arn:aws:cloudformation:eu-central-1:444265927816:stack/ECS-Console-V2-TaskDefinition-3617d9e2-87b9-4eb6-b260-f474118051ff/bc9ee3c0-7a2d-11ed-90a2-06db897b7666"
        }
    ]
}
```

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 6. Deploy the Task Definition using a Service</summary>

#### 6. Deploy the `Task Definition` using a Service

#### [Video Solution - Deploy a Task Definition to an `ECS Service`](https://www.loom.com/share/ca47334a4c3f40d496003e42a3ef72e2)

#### [Video Solution - Use `POSTMAN` to test the live `API Service` running on `ECS`](https://www.loom.com/share/487985cec38d40ac876de24e47e4785a)

#### [Video Solution - `Redeploy` the task manually when we change the code ](https://www.loom.com/share/c36e6ec8e59248c586eef82a2aed3b6d)

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 7. Automate the deployment with GitLab</summary>

#### 7. Automate the `deployment` with `GitLab`

Is time to automate our `container` deployment using `GitLab`.

##### 7.1 Setup and `AWS` user that will be used by `GitLab`

###### 7.1.1 Go to [IAM](https://us-east-1.console.aws.amazon.com/iamv2/home?region=eu-central-1)

###### 7.1.2 Head over to **Users**

![iam-users](examples/ci_cd_automate/iam_users.png)

###### 7.1.3 Click on **"Add User"**

###### 7.1.4 Choose **"Programmatic access"**

![iam-users-one](examples/ci_cd_automate/iam_add_users_step_one.png)

###### 7.1.5 Add **AmazonECS_FullAccess**

![iam-users-two](examples/ci_cd_automate/iam_add_users_step_two.png)

Make sure you have the following policies enabled for your user:

![iam-users-policy](examples/ci_cd_automate/gitalb-user-permissions.png)

###### 7.1.6 Save the user and the credentials

![iam-users-three](examples/ci_cd_automate/iam_add_users_step_three.png)

###### [Video Solution - Create a user for the `Gitlab` pipeline](https://www.loom.com/share/e7f59e9c430b45dea25e31bbc8fdbf7a)

##### 7.2 Move your `repository` to `GitLab`

Follow the instructions [in this file to move your code to GitLab.](SUPPORT.md)

##### 7.3 Create a new `pipeline`

-   Go **Pipeline Editor** and create a new pipeline
    ![gitlab-create-pipeline](examples/ci_cd_automate/gitlab-create-pipeline.png)
-   Add the following code to the pipeline

```yaml
image: node:16

# Variables Needed
# DOCKER_REGISTRY_URL -> 444265927816.dkr.ecr.eu-central-1.amazonaws.com/movie-api
# AWS_DEFAULT_REGION -> eu-central-1
# ECS_TASK_DEFINITION_FAMILY_NAME -> MovieApiProduction
# ECS_CLUSTER_NAME -> ProductionCluster
# ECS_SERVICE_NAME -> movie-api-production-service

stages:
    - build
    - test
    - package
    - deploy

build_source_code:
    stage: build
    script:
        - npm ci
        - npm run build
    artifacts:
        paths:
            - build
        expire_in: 1 week

test_job:
    stage: test
    services: # we need docker in order to be able to run the tests
        - name: docker:20.10.1-dind
          command: ['--tls=false', '--host=tcp://0.0.0.0:2376']
    variables:
        DOCKER_HOST: 'tcp://docker:2375'
        DOCKER_TLS_CERTDIR: ''
        DOCKER_DRIVER: 'overlay2'
    script:
        - export CI=true
        - npm ci
        - npm test
    needs:
        - build_source_code
    dependencies:
        - build_source_code

# build and push the Docker image to the Elastic Container Repository
build_docker_image_job:
    stage: package
    image: docker:latest
    services:
        - name: docker:dind # docker in docker image
    script:
        - apk add --no-cache curl jq python3 py-pip
        - pip install awscli
        - aws ecr get-login-password --region $AWS_DEFAULT_REGION | docker login --username AWS --password-stdin $DOCKER_REGISTRY_URL
        - docker build -t movie-api .
        - docker tag movie-api:latest $DOCKER_REGISTRY_URL:latest
        - docker push $DOCKER_REGISTRY_URL:latest

deploy_production_job:
    stage: deploy
    image: python:latest
    script:
        - pip install awscli
        - aws ecs register-task-definition --region $AWS_DEFAULT_REGION --family $ECS_TASK_DEFINITION_FAMILY_NAME --cli-input-json file://deploy/ecs_task_definition.json
        - aws ecs update-service --region $AWS_DEFAULT_REGION --cluster $ECS_CLUSTER_NAME --service $ECS_SERVICE_NAME --task-definition $ECS_TASK_DEFINITION_FAMILY_NAME
    dependencies:
        - build_docker_image_job
```

###### 🤔 We use the [AWS CLI] to create a `Task Revision` and to update the `ECS Service`. You can check the [official documentation here](https://docs.aws.amazon.com/cli/latest/reference/ecs/index.html).

##### 7.4 Setup the variables needed in `GitLab` settings

Make sure you understand what the pipeline is doing and how the variables are being injected.

![GitLab-Variables](examples/ci_cd_automate/gitlab-variables.png)

##### 7.5 Run the pipeline

The final pipeline should look like this:
![final-pipeline](examples/ci_cd_automate/gitlab-pipeline-final.png)

###### [Video Solution - Deploy to `ECS` using `GitLab`](https://www.loom.com/share/2364d4a32d1f49e58c1afbf52007e201)

**Congratulations! You deployed a service based on containers fully automatically with GitLab!**

</details>

---

#### Clean Up After the Action Item:

1) [Delete the **ECS Cluster** - Video Guide](https://www.loom.com/share/4f2af16548594dc8b67ca4fe4250dea2)
2) [Delete the **ECR Repository** - Video Guide](https://www.loom.com/share/27fa0ed1d3dd407faf800156ef8f8764)
3) [Delete the **EC2 Load Balancer** - Video Guide](https://www.loom.com/share/52484a5b148243babca7f9bc8e35e39c)
4) [Delete the **VPC Endpoints** - Video Guide](https://www.loom.com/share/545a211814a14ec8b7e903fa8b1db935)

#### ⚠️ Make sure you check the video above to avoid any unwanted charges at the end of the month! ⚠️

---

### Getting Feedback & Help

If you have issues with the Action Item, you can ask for help in the [Community](https://community.theseniordev.com/) or in the [Weekly Coaching Calls](https://calendar.google.com/calendar/u/0?cid=Y19kbGVoajU1Z2prNXZmYmdoYmxtdDRvN3JyNEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t).

### Made with :orange_heart: in Berlin by @TheSeniorDev
