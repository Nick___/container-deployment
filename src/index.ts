import dotnev from 'dotenv-flow'

// loading env variables before other imports
dotnev.config()

import createServer from './createServer'
import config from './config'
import { ApplicationServer } from './types'
import logger from './midlleware/logger'

createServer(config).then((appSever: ApplicationServer) => {
    appSever.app.listen(config.port, () => {
        logger.info(`Server is running on: http://localhost:${config.port}/`)
        logger.info(`API docs: http://localhost:${config.port}/api-docs`)
        logger.info(`GraphQL Server: http://localhost:${config.port}/graphql`)
    })
})
