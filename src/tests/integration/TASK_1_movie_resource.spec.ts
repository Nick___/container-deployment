import { Server } from 'http'
import request from 'supertest'
import { StartedTestContainer } from 'testcontainers'

import createServer from '../../createServer'
import createTestDbContainer from './createTestDbContainer'

import { v4 as uuid } from 'uuid'
import createFakeMovie from '../seed/createFakeMovie'
import { ApplicationServer } from '../../types'
import getAuthTokenTest from '../utils/getAuthTokenTest'

let app: Server,
    dbContainer: StartedTestContainer,
    appServer: ApplicationServer,
    authToken: string

beforeAll(async () => {
    // starts a Docker container with a postgres instance inside
    dbContainer = await createTestDbContainer()

    const serverConfig = {
        db: {
            port: dbContainer.getMappedPort(5432),
            host: dbContainer.getHost(),
            username: 'root',
            password: 'theSeniorDev'
        }
    }

    appServer = await createServer(serverConfig)
    app = appServer.app

    authToken = await getAuthTokenTest()
})

afterAll(async () => {
    // await appServer?.dbClient?.destroy() // disconnecting the Database Client
    await dbContainer.stop() // Stoping the Database Container
})

describe('[TASK 1] - The api/movies resource', () => {
    describe('When we make a GET request to the /api/movies endpoint', () => {
        test('Then the list of movies is returned', async () => {
            const firstMovie = await createFakeMovie()
            const secondMovie = await createFakeMovie()

            const response = await request(app)
                .get('/api/movies')
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')

            expect(response.body.length).toEqual(2)
        })
    })
    describe('When we make a GET request to api/movies/:id for a single movie', () => {
        describe('With a valid uuid', () => {
            test('Then the correct movie is returned', async () => {
                // see the database pre-seed
                const thirdMovie = await createFakeMovie()
                const response = await request(app)
                    .get(`/api/movies/${thirdMovie.id}`)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')

                expect(response.body).toMatchObject({
                    ...thirdMovie,
                    release_date: thirdMovie.release_date.toISOString()
                })
            })
        })
        describe('With an invalid uuid', () => {
            test('Then a 400 status code is returned', async () => {
                // see the database pre-seed
                const movieId = 'random-id'
                await request(app)
                    .get(`/api/movies/${movieId}`)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')
                    .expect(400)
            })
        })
        describe('With a uuid that does not exists in the database', () => {
            test('Then a 404 Not Found status code is returned', async () => {
                // see the database pre-seed
                const movieId = '05acb69a-ad9b-43fa-bea8-95e4c95e30e0'
                const response = await request(app)
                    .get(`/api/movies/${movieId}`)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')
                    .expect(404)
            })
        })
    })
    describe('Given we make a POST request to api/movies to create a movie', () => {
        describe('When the title is too long', () => {
            test('Then a 400 Bad Request code is returned to the user', async () => {
                const mockMovie = {
                    title: `
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                        Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                        It has survived not only five centuries, but also the leap into electronic typesetting, 
                        remaining essentially unchanged. It was popularised in the 1960s with the release of 
                        Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing 
                        software like Aldus PageMaker including versions of Lorem Ipsum.
                    `,
                    overview: '',
                    tagline: '',
                    runtime: '02:00:00',
                    release_date: '1994-04-04',
                    revenue: 1000000,
                    poster_path: ''
                }
                await request(app)
                    .post(`/api/movies/`)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')
                    .send(mockMovie)
                    .expect(400)

                // expect(response.body.message)
            })
        })
        describe('When the runtime has the wrong format', () => {
            test('Then a 400 Bad Request code is returned to the user', async () => {
                const mockMovie = {
                    title: `
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                    `,
                    overview: '',
                    tagline: '',
                    runtime: '02:00',
                    release_date: '1994-04-04',
                    revenue: 1000000,
                    poster_path: ''
                }
                await request(app)
                    .post(`/api/movies/`)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')
                    .send(mockMovie)
                    .expect(400)

                // expect(response.body.message)
            })
        })
        describe('When the input is valid', () => {
            test('Then the movie is created', async () => {
                const mockMovie = {
                    title: 'Porco Rosso',
                    overview: `In Italy in the 1930s, sky pirates in biplanes terrorize wealthy cruise ships as they sail the Adriatic Sea. 
                        The only pilot brave enough to stop the scourge is the mysterious Porco Rosso, a former World War I flying ace who was somehow turned into a pig during the war. As he prepares to battle the pirate crew-s American ace, 
                        Porco Rosso enlists the help of spunky girl mechanic Fio Piccolo and his longtime friend Madame Gina.`,
                    tagline: 'A Pig Got to Fly.',
                    runtime: '01:34:00',
                    release_date: '1992-07-18',
                    revenue: 44600000,
                    poster_path:
                        'https://www.themoviedb.org/t/p/w600_and_h900_bestv2/byKAndF6KQSDpGxp1mTr23jPbYp.jpg'
                }
                const response = await request(app)
                    .post(`/api/movies/`)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')
                    .send(mockMovie)
                    .expect(200)

                expect(response.body).toMatchObject(mockMovie)
            })
        })
    })
    describe('Given we make a PATCH request to api/movies/:movieId to update a movie', () => {
        describe('When the id provided is not valid', () => {
            test('A 400 code is returned', async () => {
                const id = 'invalid-uuid'
                await request(app)
                    .patch(`/api/movies/${id}`)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')
                    .expect(400)
            })
        })
        describe('When a movie with the id provided does not exists', () => {
            test('A 404 code is returned', async () => {
                const id = uuid()
                await request(app)
                    .patch(`/api/movies/${id}`)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')
                    .expect(404)
            })
        })
        describe('When the input is valid', () => {
            test('Then the updated movie is returned to the user', async () => {
                // create movie
                const fakeMovie = await createFakeMovie()

                const movieUpdate = {
                    revenue: 519000
                }

                const updatedMovieResponse = await request(app)
                    .patch(`/api/movies/${fakeMovie.id}`)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')
                    .send(movieUpdate)
                    .expect(200)

                expect(updatedMovieResponse.body).toMatchObject({
                    ...fakeMovie,
                    ...movieUpdate,
                    release_date: fakeMovie.release_date.toISOString()
                })
            })
        })
    })

    describe('Given we make a DELETE request to api/movies/:movieId to delete a movie', () => {
        describe('When the id provided is not valid', () => {
            test('A 400 code is returned', async () => {
                const id = 'invalid-uuid'
                await request(app)
                    .delete(`/api/movies/${id}`)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')
                    .expect(400)
            })
        })
        describe('When a movie with the id provided does not exists', () => {
            test('A 404 code is returned', async () => {
                const id = uuid()
                await request(app)
                    .delete(`/api/movies/${id}`)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')
                    .expect(404)
            })
        })
        describe('When the uuid is valid and the movie exists', () => {
            test('Then the movie is deleted succesfully', async () => {
                // create movie
                const mockCreateMovie = {
                    title: `Kiki's Delivery Service`,
                    overview: `
                       A young witch, on her mandatory year of independent life, finds fitting into a new community difficult while 
                       she supports herself by running an air courier service.
                    `,
                    tagline: `I was feeling blue, but I'm better now.`,
                    runtime: '01:43:00',
                    release_date: '1989-07-29',
                    revenue: 449017,
                    poster_path:
                        'https://www.themoviedb.org/t/p/w600_and_h900_bestv2/pN4iKcvpyxkhtyOKUKe2tTqEDYm.jpg'
                }

                // Creating the movie
                const createdMovieResponse = await request(app)
                    .post(`/api/movies/`)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')
                    .send(mockCreateMovie)
                    .expect(200)

                // Deleting the movie
                await request(app)
                    .delete(`/api/movies/${createdMovieResponse.body.id}`)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')
                    .expect(204) // Successfull delete with no content in the body

                // Checking the movie was deleted
                await request(app)
                    .get(`/api/movies/${createdMovieResponse.body.id}`)
                    .auth(authToken, { type: 'bearer' })
                    .set('Content-Type', 'application/json')
                    .expect(404)
            })
        })
    })
})
