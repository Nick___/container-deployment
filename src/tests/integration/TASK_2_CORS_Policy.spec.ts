import { Server } from 'http'
import request from 'supertest'
import { StartedTestContainer } from 'testcontainers'

import createServer from '../../createServer'
import { ApplicationServer } from '../../types'
import getAuthTokenTest from '../utils/getAuthTokenTest'
import createTestDbContainer from './createTestDbContainer'

let app: Server,
    dbContainer: StartedTestContainer,
    appServer: ApplicationServer,
    authToken: string

beforeAll(async () => {
    // starts a Docker container with a postgres instance insie
    dbContainer = await createTestDbContainer()

    const serverConfig = {
        db: {
            port: dbContainer.getMappedPort(5432), // random
            host: dbContainer.getHost(),
            username: 'root',
            password: 'theSeniorDev'
        }
    }

    appServer = await createServer(serverConfig)
    app = appServer.app

    authToken = await getAuthTokenTest()
})

afterAll(async () => {
    await appServer?.dbClient?.destroy() // disconnecting the Database Client
    await dbContainer.stop() // Stoping the Database Container
})

describe('[TASK 2] - The API responses have the right CORS headers', () => {
    describe('When a request is made to the api/movie resources', () => {
        const validMovieId = 'f74cf1ca-8c7b-435b-96c6-e4448a653596'

        test('It contains the right CORS Allow Origin', async () => {
            const response = await request(app)
                .get(`/api/movies/${validMovieId}`)
                .auth(authToken, { type: 'bearer' })
                .set('Content-Type', 'application/json')
            expect(response.headers['access-control-allow-origin']).toEqual('*')
        })

        /*
        test('It contains the right CORS Allowed Methods', async () => {
            const response = await request(app)
                .get(`/api/movies/${validMovieId}`)
                .auth(token, { type: 'bearer' })
                .set('Content-Type', 'application/json')

            expect(response.headers['access-control-allow-methods']).toEqual(
                'POST, GET, PATCH, DELETE'
            )
        })*/
    })
})
