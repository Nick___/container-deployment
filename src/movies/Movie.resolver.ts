import Joi from 'joi'
import { DeepPartial } from 'typeorm'
import { ControllerError } from '../types'
import MovieController from './Movie.controller'
import MovieModel from './Movie.model'
import { MovieCreateDTO } from './Movie.types'
import { MovieCreateInputSchema } from './Movie.validators'
import logger from '../midlleware/logger'

export default {
    reviews: async (root: MovieModel) => {
        const movieWithReviews = await MovieController.getMovieWithReviews(
            root.id
        )
        return movieWithReviews?.reviews
    }
}

export const movieMutations = {
    movieCreate: async (
        root: undefined,
        args: { input: MovieCreateDTO }
    ): Promise<MovieModel | Joi.ValidationError | ControllerError> => {
        const result = MovieCreateInputSchema.validate(args.input)
        try {
            if (result.error) {
                return result.error
            } else {
                const newMovie = await MovieModel.save(
                    args.input as DeepPartial<MovieModel>
                )
                return newMovie
            }
        } catch (error) {
            logger.error(error)
            return {
                error_id: 'CREATE_MOVIE',
                message: 'An internal error has occurred.'
            }
        }
    }
}
