import Joi from 'joi'
import { BaseEntity } from 'typeorm'
import { ControllerError } from '../types'
import {
    MovieCreateInputSchema,
    MovieUpdateInputSchema
} from './Movie.validators'
import MovieModel from './Movie.model'
import logger from '../midlleware/logger'

export default {
    async getMovieList(): Promise<MovieModel[]> {
        return await MovieModel.find()
    },
    async getMovieWithReviews(movieId: string): Promise<MovieModel | null> {
        return await MovieModel.findOne({
            where: { id: movieId },
            relations: { reviews: true }
        })
    },
    async getMovieById(movieId: string): Promise<MovieModel | null> {
        return MovieModel.findOneBy({ id: movieId })
    },
    async deleteMovieById(id: string): Promise<true | ControllerError> {
        try {
            await MovieModel.delete(id)
            return true
        } catch (error) {
            return {
                error_id: 'DELETE_MOVIE',
                message: 'An internal error has occurred.'
            }
        }
    },
    async createMovie(
        movieCreateData: Partial<MovieModel>
    ): Promise<BaseEntity | Joi.ValidationError | ControllerError> {
        const result = MovieCreateInputSchema.validate(movieCreateData)
        if (result.error) {
            return result.error
        } else {
            try {
                const newMovie = await MovieModel.save(movieCreateData)
                return newMovie
            } catch (error) {
                logger.error(error)
                return {
                    error_id: 'CREATE_MOVIE',
                    message: 'An internal error has occurred.'
                }
            }
        }
    },
    async updateMovieById(
        movieId: string,
        movieUpdateData: Partial<MovieModel>
    ): Promise<MovieModel | Joi.ValidationError | ControllerError | null> {
        const result = MovieUpdateInputSchema.validate(movieUpdateData)
        if (result.error) {
            return result.error
        } else {
            try {
                const movie = await MovieModel.findOneBy({ id: movieId })

                if (!movie) {
                    return {
                        error_id: 'UPDATE_MOVIE_NOT_FOUND',
                        message: 'An internal error has occurred.'
                    }
                }

                await MovieModel.update(movieId, movieUpdateData)

                const updatedMovie = await MovieModel.findOneBy({
                    id: movieId
                })

                return updatedMovie
            } catch (error) {
                logger.error(error)
                return {
                    error_id: 'UPDATE_MOVIE_INTERNAL_ERROR',
                    message: 'An internal error has occurred.'
                }
            }
        }
    }
}
