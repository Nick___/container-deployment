export const permissionsType = {
    moviesRead: 'read:movies',
    moviesWrite: 'write:movies',
    movieReviewsRead: 'read:movie-reviews',
    movieReviewsWrite: 'write:movie-reviews'
}
