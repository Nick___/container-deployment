import express, { Response, Request } from 'express'
import swaggerUi from 'swagger-ui-express'
import cors from 'cors'
import http from 'http'
import compression from 'compression'
import { ApolloServer } from 'apollo-server-express'
import { ApolloServerPluginLandingPageGraphQLPlayground } from 'apollo-server-core'
import { loadSchemaSync } from '@graphql-tools/load'
import { GraphQLFileLoader } from '@graphql-tools/graphql-file-loader'
import { addResolversToSchema } from '@graphql-tools/schema'
import { express as voyagerMiddleware } from 'graphql-voyager/middleware'
import { expressMiddleware } from '@apollo/server/express4';


import { ApplicationServer, ServerConfig } from './types'
import { loggerMiddleware } from './midlleware/logger'
import { swaggerDocument, swaggerOptions } from './midlleware/swaggerOptions'
import createDbConnection from './database/createDbConnection'
import movieRouter from './movies/Movie.router'
import movieReviewRouter from './movieReviews/MovieReview.router'
import authenticationMiddleware from './midlleware/authentication'
import resolver from './graphql/query.resolver'

async function createServer(config: ServerConfig): Promise<ApplicationServer> {
    const app = express()

    // Application Dependencies
    const dataSourceInstance = await createDbConnection(config.db)

    // Middlewares
    app.use(express.json()) // To parse the incoming requests with JSON payloads
    app.use(cors())
    if (process.env.NODE_ENV !== 'test') {
        app.use(loggerMiddleware)
    }
    app.use(compression())

    app.use('/api', authenticationMiddleware)

    // Swagger docs
    app.get('/api-docs/swagger.json', (req, res) => res.json(swaggerDocument))
    app.use(
        '/api-docs',
        swaggerUi.serveFiles(undefined, swaggerOptions),
        swaggerUi.setup(undefined, swaggerOptions)
    )

    // voyager
    app.use('/voyager', voyagerMiddleware({ endpointUrl: '/graphql' }))

    // nested routing
    movieRouter.use('/:movieId/reviews', movieReviewRouter)

    // Movie Router
    app.use('/api/movies', movieRouter)

    // Container Healthcheck
    app.get('/healthcheck', (_request: Request, response: Response) => {
        response.json({ status: 'OK' })
    })

    // api version for test
    app.get('/version', (_request: Request, response: Response) => {
        response.json({ status: 'v1.14' })
    })

    // GraphQL
    // A map of functions which return data for the schema.

    // The GraphQL schema
    const schema = loadSchemaSync(__dirname, {
        loaders: [new GraphQLFileLoader()]
    })

    const httpServer = http.createServer(app)

    const schemaWithResolvers = addResolversToSchema({
        schema,
        resolvers: resolver
    })

    const server = new ApolloServer({
        schema: schemaWithResolvers,
        plugins: [ApolloServerPluginLandingPageGraphQLPlayground()]
    })


    return {
        app: httpServer,
        dbClient: dataSourceInstance
    }
}

export default createServer
