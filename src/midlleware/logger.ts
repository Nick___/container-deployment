import winston from 'winston'
import expressWinston from 'express-winston'

const productionFormat = winston.format.combine(
    winston.format.json(),
    winston.format.timestamp()
)

const developmentFormat = winston.format.combine(
    winston.format.json(),
    winston.format.timestamp(),
    winston.format.prettyPrint()
)

const loggerOptions = {
    transports: [new winston.transports.Console()],
    format:
        process.env.NODE_ENV === 'production'
            ? productionFormat
            : developmentFormat,
    meta: true,
    colorize: false,
    expressFormat: true,
    headerBlacklist: ['Authorization']
}

// normal logger instance
const logger = winston.createLogger(loggerOptions)

// http logger middleware
export const loggerMiddleware = expressWinston.logger(loggerOptions)

// use always instead of console.log
export default logger
