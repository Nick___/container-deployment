export type MovieReviewCreateDTO = {
    author_name: string
    content: string
    rating: number
    movie_id: string
}
