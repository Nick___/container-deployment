import Joi from 'joi'
import { ControllerError } from '../types'
import MovieReviewController from './MovieReview.controller'
import MovieReview from './MovieReview.model'
import { MovieReviewCreateInputSchema } from './MovieReview.validators'
import { MovieReviewCreateDTO } from './MovieReviews.types'
import logger from '../midlleware/logger'

export const movieReviewMutations = {
    movieReviewCreate: async (
        root: undefined,
        args: { input: MovieReviewCreateDTO }
    ): Promise<MovieReview | Joi.ValidationError | ControllerError> => {
        const result = MovieReviewCreateInputSchema.validate(args.input)
        try {
            if (result.error) {
                return result.error
            } else {
                logger.log(args.input.movie_id, args.input)
                const newMovieReview =
                    await MovieReviewController.createMovieReview(
                        args.input.movie_id,
                        args.input
                    )
                return newMovieReview
            }
        } catch (error) {
            logger.error(error)
            return {
                error_id: 'CREATE_MOVIE_REVIEW',
                message: 'An internal error has occurred.'
            }
        }
    }
}
