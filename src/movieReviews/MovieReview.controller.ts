import Joi from 'joi'
import { ControllerError } from '../types'
import {
    MovieReviewCreateInputSchema,
    MovieReviewUpdateInputSchema
} from './MovieReview.validators'
import logger from '../midlleware/logger'
import MovieReviewModel from './MovieReview.model'
import MovieController from '../movies/Movie.controller'

export default {
    async getMovieReviewList(): Promise<MovieReviewModel[]> {
        return await MovieReviewModel.find()
    },
    async getMovieReviewById(
        reviewId: string
    ): Promise<MovieReviewModel | null> {
        return MovieReviewModel.findOneBy({ id: reviewId })
    },
    async deleteMovieReviewById(
        reviewId: string
    ): Promise<true | ControllerError> {
        try {
            await MovieReviewModel.delete(reviewId)
            return true
        } catch (error) {
            return {
                error_id: 'DELETE_MOVIE_REVIEW',
                message: 'An internal error has occurred.'
            }
        }
    },
    async createMovieReview(
        movieId: string,
        movieReviewCreateData: Partial<MovieReviewModel>
    ): Promise<MovieReviewModel | Joi.ValidationError | ControllerError> {
        const result = MovieReviewCreateInputSchema.validate(
            movieReviewCreateData
        )
        if (result.error) {
            return result.error
        } else {
            try {
                const newMovieReview = (await MovieReviewModel.create(
                    movieReviewCreateData
                )) as MovieReviewModel

                // fething the movie
                const movie = await MovieController.getMovieWithReviews(movieId)

                if (movie) {
                    movie.reviews = [
                        ...movie.reviews,
                        newMovieReview
                    ] as MovieReviewModel[]

                    // saving the movie review
                    await newMovieReview.save()

                    // saving teh movie
                    movie?.save()

                    // returning the moview review
                    return newMovieReview
                } else {
                    return {
                        error_id: 'MOVIE_NOT_FOUND',
                        message: 'An internal error has occurred.'
                    }
                }
            } catch (error) {
                logger.error(error)
                return {
                    error_id: 'CREATE_MOVIE_REVIEW',
                    message: 'An internal error has occurred.'
                }
            }
        }
    },
    async updateMovieReviewById(
        reviewId: string,
        movieUpdateData: Partial<MovieReviewModel>
    ): Promise<
        MovieReviewModel | Joi.ValidationError | ControllerError | null
    > {
        const result = MovieReviewUpdateInputSchema.validate(movieUpdateData)
        if (result.error) {
            return result.error
        } else {
            try {
                const movie = await MovieReviewModel.findOneBy({ id: reviewId })

                if (!movie) {
                    return {
                        error_id: 'UPDATE_MOVIE_NOT_FOUND',
                        message: 'An internal error has occurred.'
                    }
                }

                await MovieReviewModel.update(reviewId, movieUpdateData)

                const updatedMovie = await MovieReviewModel.findOneBy({
                    id: reviewId
                })

                return updatedMovie
            } catch (error) {
                logger.error(error)
                return {
                    error_id: 'UPDATE_MOVIE_REVIEW_INTERNAL_ERROR',
                    message: 'An internal error has occurred.'
                }
            }
        }
    }
}
